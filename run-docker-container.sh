#!/bin/bash

set -ex

# sudo docker run --rm -p 4000:4000 wtfleming/compojure-hello-world
docker run --rm --name app -p 4000:4000 -p 6379:6379 app
