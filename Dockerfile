#
# Dockerfile for a compojure hello world app
#
FROM clojure-base

ADD config/redis.conf /etc/supervisor/conf.d/
ADD config/app.conf /etc/supervisor/conf.d/
RUN ls /etc/supervisor/conf.d

RUN mkdir -p /var/app
RUN mkdir -p /var/log/app
RUN mkdir -p /var/log/redis

ADD hello-world-0.1.0-standalone.jar /var/app/app.jar

RUN ls /var/app
RUN ls /var/log/app
RUN ls /var/log/redis

ENV PORT 4000

EXPOSE 4000

# CMD ["java", "-jar", "/var/app/app.jar"]
# CMD ["redis-server"]
# CMD ["supervisord", "-c", "/etc/supervisor/supervisord.conf", "-d", "/etc/supervisor/conf.d/"]
CMD ["supervisord", "-c", "/etc/supervisor/supervisord.conf", "-n"]