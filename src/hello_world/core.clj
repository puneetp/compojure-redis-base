(ns hello-world.core
  (:require [compojure.core :refer :all]
            [org.httpkit.server :refer [run-server]]
            [ring.middleware.params :as params])
  (:gen-class))

(defn url-form-decoder [request]
  (println (params/assoc-query-params request "UTF-8")))

(defroutes myapp
  ;; (GET "/api" [] "Hello World")
  ;; (GET "/incoming" [] "incoming")
  (GET "/" request url-form-decoder))

(defn -main []
  (let [port (Integer/parseInt (or (System/getenv "PORT") "8080"))]
    (run-server myapp {:port port})
    (println (str "Listening on port " port))))
